# Adding languages.

## F123Light

Add the new language to the languages array in build/F123Light.conf, for example:

    'es_ES.UTF-8'


## files-F123Light

In select language, add the new language to the array. Make sure the defaults will work for the new language, if not add to the case statement so that files will be properly configured.


