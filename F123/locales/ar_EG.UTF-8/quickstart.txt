Welcome to the F123Light Operating System!

Since this is the first time you start your computer with F123Light, we will share with you introductory information on the screen reader, as well as present a few questions which will help you configure your system.

The screen reading software reading this text is called Fenrir. Many commands that control Fenrir involve a special key, which we call the Fenrir key. The Fenrir key is the "Insert" key on your numeric keypad, tipically found on the right of most full-size desktop keyboards.

You can hold "Fenrir+Control" and use the left and right arrow keys to select speech settings such as pitch, rate, or volume. Hold "Fenrir+Control" and use the up and down arrows to adjust the selected speech setting.

To read by line, you can use the 7, 8, and 9 keys on your numeric keypad.  Number 7 will read the previous line, 8 the current line, and 9 the next line.

If you find that something is not reading like it should, try using highlight mode. You can access this by pressing "Fenrir+numpad_star". One place where this is useful is the menu in the text editor. To get back to cursor tracking, the default, simply press the same shortcut again.

In the event the system stops speaking completely, you can use the Fenrir script key, sometimes called the Windows or Super key, in combination with shift+r to run the restore-accessibility command. This should almost never be needed, but is in place to provide a rescue should a configuration file get changed to an unusable state by accident.

You may explore these and other commands without causing any changes to the computer by entering Fenrir's learn mode by pressing "Fenrir+H". After entering "Fenrir+H", you can press any key combination you desire and the computer  will describe its purpose. Once you press the "Escape" key, the screen will be  active again and any key combination you press will actually work.

Feel free to try "Insert+H" now, if you wish to explore the keyboard without causing this screen to change.

To move to the next screen, press either the enter or escape key. The "Escape" key or "ESC" key is usually located on the top left corner of the keyboard.


This introduction will now allow you to easily configure your new system. For any questions which you do not know the answer for or would prefer to answer at another time, press the "Escape" key.

For security reasons, we recommend that you start by changing the default  passwords to something that is actually secret.

Start by changing the password you used to login. This password is also used  for administration tasks such as configuring your wireless network.

To continue with the setup process, press the "Enter" key.

