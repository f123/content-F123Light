﻿Bem-vindo ao Sistema Operacional F123Light!

Como é a primeira vez que você inicia seu computador com o F123Light, daremos a você informações introdutórias sobre o leitor de telas e faremos algumas perguntas que o ajudarão a configurar o sistema.

O software leitor de telas que lê este texto é chamado Fenrir. Muitos comandos que controlam o Fenrir envolvem uma tecla especial, que por vezes chamamos de Tecla Fenrir. A Tecla Fenrir é a tecla "Insert" do teclado numérico, que se encontra normalmente à direita na maioria dos teclados de desktop de tamanho padrão.

Por exemplo, manter "Fenrir" pressionada e apertar a Seta Cima aumenta o volume. "Fenrir+Seta Baixo" diminui o volume. Pode usar também "Fenrir+Esquerda" e "Fenrir+Direita" para reduzir e aumentar a velocidade com que o Fenrir lê a tela para você.

Para ler por linha, pode usar as teclas 7, 8 e 9 do teclado numérico.  O número 7 lê a linha anterior, 8 a linha atual e 9 a próxima linha.

Você pode explorar estes e outros comandos sem fazer qualquer alteração no computador entrando no modo de aprendizado do Fenrir, pressionando "Fenrir+1". Após apertar "Fenrir+1",. Você pode pressionar qualquer combinação de teclas que desejar e o computador descreverá o propósito da mesma. Assim que você pressionar a tecla "Escape", a tela ficará ativa novamente e qualquer tecla que você pressionar vai realmente funcionar.

In the event the system stops speaking completely, you can use the Fenrir script key, sometimes called the Windows or Super key, in combination with shift+r to run the restore-accessibility command. This should almost never be needed, but is in place to provide a rescue should a configuration file get changed to an unusable state by accident.

You may explore these and other commands without causing any changes to the computer by entering Fenrir's learn mode by pressing "Fenrir+H". After entering "Fenrir+H", you can press any key combination you desire and the computer  will describe its purpose. Once you press the "Escape" key, the screen will be  active again and any key combination you press will actually work.

Feel free to try "Insert+H" now, if you wish to explore the keyboard without causing this screen to change.

To move to the next screen, press either the enter or escape key. The "Escape" key or "ESC" key is usually located on the top left corner of the keyboard.


This introduction will now allow you to easily configure your new system. For any questions which you do not know the answer for or would prefer to answer at another time, press the "Escape" key.

For security reasons, we recommend that you start by changing the default  passwords to something that is actually secret.

Start by changing the password you used to login. This password is also used  for administration tasks such as configuring your wireless network.

To continue with the setup process, press the "Enter" key.

