Initial F123Light configuration is now complete.

If you wish to further customize your computer, please open 'Settings' on the main menu. You can reach the main menu by pressing Ctrl+Escape or Alt+F1 after this computer restarts.

The "Settings" menu also has an easy option to update F123Light, but we strongly recommend that you only update this system when a new version is announced on our e-mail lists. To learn more about our project and subscribe to our lists, please visit our guide at:
https://guide.f123.org/

Remember, reader+Ctrl+G takes you to screen reader settings and reader+Ctrl+arrows allows for quick changes to speech volume, rate, and pitch.

Please press 'Enter' to restart this computer.

