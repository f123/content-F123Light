You can use Control+Home and Control+End to quickly go to the top and bottom of the document.
You can use Home and End to quickly go to the beginning and end of the current line.
You can use Alt+W to hear how many words, lines, and characters your document has.
You can use F7 to spell check your document.
You can use Alt+Shift+Less than and Alt+Shift+Greater than to move to other documents which are opened on the text editor.
You can use Control+H to search and replace words, strings, or patterns.
You can use Control+F to search and find something on your document.
You can use Alt+C to let the editor try to complete a word you have started typing. You must have used that same word somewhere else on the same document.
You can hold down the Shift key and then move the cursor to highlight any text and then copy it with Control+C or cut it with Control+X. Then you can paste it in a new location in that document with Control+V.
If you have more than one document opened at the same time, you can use Alt+Spacebar to go to the next document or Control+Spacebar to go to the previous document.
You can use Control+O to open a new document.
In some places on this editor, the word "buffer" is used in place of the word "file", but it refers to the same thing.
