Você pode usar Control+Home e Control+End para ir rapidamente ao topo e ao fim do documento.
Pode usar Home e End para ir rapidamente ao início e ao fim da linha atual.
Pode usar Alt+W para ouvir quantas palavras, linhas e caracteres o documento possui.
Pode usar F7 para verificar erros de ortografia no documento.
Pode usar Alt+Shift+menor e Alt+Shift+maior para mover-se para outros documentos que estejam abertos no editor de texto.
Pode usar Control+H para localizar e substituir palavras, cadeias ou padrões.
Pode usar Control+F para procurar e achar alguma coisa no documento.
Pode usar Alt+C para que o editor tente completar uma palavra que você começou a digitar. Você já tem que ter usado essa mesma palavra em algum lugar do mesmo documento.
Pode manter pressionada a tecla Shift e então mover o cursor para selecionar algum texto e aí copiá-lo com Control+C ou cortá-lo com Control+X. Aí pode colá-lo num novo local do documento com Control+V.
Caso você tenha mais de um documento aberto ao mesmo tempo, pode usar Alt+Barra de Espaço para ir ao próximo documento ou Control+Barra de Espaço para ir ao documento anterior.
Pode usar Control+O para abrir um novo documento.
Em algumas partes deste editor, a palavra "buffer" é usada no lugar da palavra "arquivo", mas ela se refere à mesma coisa.
