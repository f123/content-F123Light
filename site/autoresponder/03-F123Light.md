If you would like to learn more about the world's most affordable fully-functional talking computer for the blind, please visit our F123Light page at:
https://guide.f123.org/

If you have software-related questions, suggestions, comments, and contributions, please join one of our e-mail lists at:
https://guide.f123.org/getting-started.html#our-e-mail-lists

To volunteer, help, and contribute in various ways, please look at:
https://guide.f123.org/getting-started.html#contributing-time-talent-contacts-etc

To download the latest version of the F123Light operating system, please visit:  
https://guide.f123.org/getting-started.html#download

