## Social Impact and Value Versus Price Confusion

The high relative and absolute cost of most assistive technologies negatively impact the blind at multiple levels. In addition to restricting immediate access to software and devices, it can also have negative consequences even when funding is obtained to purchase the needed technology.

The classic model in most non-profits is to request funding from government agencies or foundations, purchase equipment and software, hire a teacher, and then offer training for the blind. This approach quickly fails in its greater goal of improving access to education and employment for the blind, since as soon as the training is finished, it becomes abundantly clear that neither the blind person, his or her family or school, nor local companies are able to afford the technology in which training was offered.

When nobody in the local community can afford to purchase the selected technology, it also means that even the rare donation is not usually maintained or replaced when necessary. As a result costs with virus protection, backups, and physical upgrades, as well as maintenance and security cannot be absorbed, and the donated technology becomes progressively less useful and the users increasingly less productive. Since talking computers can have such a substantial impact on the productivity and competitiveness of the blind, their malfunction can also close the doors that might have been opened with great effort.

In sum, the "best" technology is a relative concept, and the choice of the most expensive options rarely if ever substitute for a careful analysis of local circumstances. This is the reason why so many pilot projects are never scaled-up and so many initiatives fail as soon as external funding ends.

