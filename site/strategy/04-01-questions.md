# Common Questions and Suggestions

Questions and suggestions are always welcome. The following are some of the more common questions and suggestions we receive, and we are sharing them here as these might be helpful in focusing your creativity in new areas. Please always feel free to ask new questions, request clarifications, or make suggestions on [our e-mail lists or directly with our team](https://guide.f123.org/getting-started.html#finding-or-offering-help).

