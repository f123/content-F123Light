## Buying Versus Assembling the F123Light Computer

Some people have asked if they can buy one of these talking computers already assembled and with the F123Light software preinstalled:  

We can assemble the computer in Brazil and ship to anywhere in the world. So [get in touch with us via e-mail](https://guide.f123.org/getting-started.html#contacting-our-team-directly) if you need one urgently. In the next few months we will also setup a partnership with an international manufacturer. As soon as that is finalized, we will provide links to their online store in our pages.

People who are blind and wish to become local distributors, should get in touch with us via e-mail.

