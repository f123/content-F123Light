## Using Smartphones Instead of the Raspberry Pi Computer

We are frequently asked why we are not using smartphones instead of Raspberry Pi computers, as smartphones are so powerful, affordable, and widely available today. While we do not automatically exclude the possibility of using smartphones in the future, the reasons why we believe it is not currently a better alternative to the Raspberry Pi for young blind students and others are:  

* Competitive pressures force manufacturers to continuously update hardware and software features on phones, so any scarce resources invested in adapting these devices to productive and blind-accessible educational uses, are quickly lost due to the rapid changes in phone operating systems and the unavailability of slightly older phone models.
* Smartphones are optimized in every way for the visual consumption of information and their lack of keyboards and delicate glass screens make them sub optimal for the daily use of children in a school setting. These phones are too easily damaged or broken, and if used without major software changes, become distraction machines rather than educational tools.
* Competitive pressures also foster phone designs which do not allow the end-user to exchange the battery, so the cost of medium to long-term maintenance is unnecessarily high, and probably significantly higher than that of a Raspberry Pi that uses a phone charger or power bank.
* There is ample evidence that due to the extreme competitive pressures faced by the smartphone industry, too often either phone manufacturers or phone companies install software that tracks and monitors users in order to display advertising, or sell user location information to commercial entities, or both. This seems to be especially true with the most affordable phone models. We believe that this is undesirable for users of any age, and entirely unacceptable for children.

Our choice of the Raspberry Pi as a platform, minimizes distractions, as we have greater control over what software is installed. Greater security, as neither the manufacturer nor the phone company have a chance to install tracking and advertising software of any kind. Finally, even though the Raspberry Pi hardware evolves, it retains its technical patterns for a much longer time period, extending the usefulness of our work for much longer.

In sum, affordable phones based on the Android operating system are not durable, are not secure from digital attacks, are expensive to maintain in realistic day-to-day use by children, offer too many distractions if the operating system is not entirely modified, and change too often as a platform for most software development investment to continue being useful across time and phone models.

We have also been asked about recycling discarded tablets for use by the blind, and many of the same issues described above also apply in that case.

