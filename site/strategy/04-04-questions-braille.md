## Using Braille with the F123Light Computer

We have been asked if the F123Light computer is compatible with Braille displays available for use with various electronic devices ranging from laptops to smartphones and conventional computers:  

Since our initial focus was to create the world's most affordable fully functional talking computer for the blind, we did not include a Braille display in our first few prototypes. However, we have been asked for this option and will start researching low-cost Braille displays which we can include in the near future. We expect that integrating any of the major brands into our device will not be difficult.

Once the Braille display option becomes available, we will announce it in our e-mail lists and web pages.

