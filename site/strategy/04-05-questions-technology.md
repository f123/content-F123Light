## Technology Cannot be the Main Focus in Education

Sometimes it is said that our societies are giving too much emphasis to technological tools rather than values and personal development:  

We believe that no technology can substitute for personal growth and a proper education. This is why we give as much emphasis to training as we do to actual software development. Good technology is that which becomes invisible so that children can study and adults can work, without worrying or being distracted by the digital tools they might be using.

We also believe strongly that at the end of the day, it is not our tools but our personalities and values that actually have the greatest influence in our lives, so in some ways, our technological strategy is just a smaller side-effect from a greater objective, which is to give a chance for the blind to learn to use, maintain, and improve their own tools, and have the confidence and generosity of spirit to teach one another as well. A good example of this is our campaign to teach the blind to help one-another to build their own talking computers in workshops all over the world:  
<https://www.youtube.com/watch?v=KBM8Nq-f2T4&feature=youtu.be>

