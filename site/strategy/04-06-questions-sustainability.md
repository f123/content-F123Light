## Where is the Catch?

It is understandable that an often unasked but important question is: How can you survive if you distribute your software for free and anyone can assemble this computer from widely available standard parts?

The simple and short answer is that we have been around for about a decade now, offering project management and consulting services to organizations, foundations, and government agencies interested in helping the blind in an effective, efficient, and scalable way, and will probably continue to be around for many more years. However, our strategy is designed to ensure that no dependency is created on F123 Consulting.

The more complete answer is that when one uses hardware that is generic and available everywhere from multiple manufacturers and distributors; software that is freely available world-wide and licensed in a way that allows for distribution, modification, and copying by anyone (i.e. open source); and documentation and other materials also licensed in a similar way (i.e. [Creative Commons](https://creativecommons.org/licenses/by-nc-sa/4.0/)); there is nothing to stop anyone from continuing our work whether we are still here or have long since stopped providing our services. In fact, decentralized replication and continuation is part of our plan.

In sum, most mathematics teachers or cooks do not make a living because they teach secret formulas or cook secret recipes, they make a living because enough people do not have time to teach mathematics to their own kids, and do not have time to cook their own meal. Of course, it is not a strategy or business model that is conducive to making excessive amounts of money, but then, there are more important things in life.

Of course, we do ask that you help society reward this type of courage by [contributing to this initiative in some way](https://guide.f123.org/getting-started.html#contributing-time-talent-contacts-etc).

