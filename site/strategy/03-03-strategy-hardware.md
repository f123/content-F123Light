## Our Hardware Choices

Two important challenges faced by projects who want to make it easy for non-technical persons to enjoy the benefits of free and open source software are distribution and installation. The F123 team is addressing these obstacles by helping technically-inclined blind persons support other less-technical blind persons, and by standardizing our work around specific hardware platforms. At this time, our software is designed to work with the [Raspberry Pi 3 Model B+](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/).

Of course, our software can and has been used in other computers, but the decision to standardize on the Raspberry Pi 3 Model B+, which is sufficiently powerful for productive use of text-based applications, saves a lot of time and effort for both the development team and our users. Issues with software drivers are resolved more easily, testing is simpler, efficient distribution is already taken care of, and potential help from sighted users of these devices already exists around the world.

### List of Computer Parts

Since extreme affordability is absolutely essential, [every part selected for use in our project is both produced and distributed world-wide in mass](https://f123.org/partes-f123light). In addition to the Raspberry Pi computer, we also use:

@. A Micro SD memory card.

This is the computer's hard disk.

@. Heat sinks and computer case.

Parts which protect the circuit board and help cool it.

@. AC Adapter.

Allows you to connect the computer to the electrical outlet.

@. Power Pack or Battery Pack.

Many versions of this part exist. These are usually used to recharge cell phones, but we can use it to power the talking computer for many hours.

@. Standard USB keyboard.

@. Standard headphones with 3.5mm audio jack.

There are of course, Bluetooth keyboards and headphones, with a wide variety of prices and reliability, but in our experience, none of those are as affordable, durable, and easy-to-find and maintain as the traditional wired originals.

Actually, there is one exception to the rule of only using widely available parts and that is the computer carrying case. This part is very helpful, but at the same time, in a worse-case scenario, importing it across continents can be considered somewhat optional. Any person or organization with experience working with textiles should be able to put together a carrying case, and we have heard of users carrying their talking computer even in plastic shopping bags.

Finally, what brings it all together and makes this device so helpful is our F123Light software, which combines a wide variety of free and open source software, many developed, maintained, and used by organizations big and small for purposes which often have nothing to do with blindness. In other words, we have designed the entire project to minimize costs, and maximize productivity, sustainability, decentralization, and longevity.

