# Defining the Problem

Improving access to education and employment opportunities for the blind is, as is the case with most social challenges, a complex problem which involves a variety of social, economic, and technological factors. The following are some of the problems we have identified as important and which we hope to address through our F123Light initiative.

