## Updating F123Light on the Command Line

Advanced users can perform an update from the command line with the command:

    update-f123light

If such a user is helping us test recent changes to the code, he or she can use a special parameter to update from the development branch. To specify the development branch, the correct command would be:

    update-f123light -b dev

