# Downloading, Updating, and Building Images

## Downloading the Latest Release Candidate Image

Experienced users may wish to assist us in testing new features and updates. One way to do this is by downloading and trying the latest release candidate image.

You may find our latest release candidate here:  
<https://public.f123.org/releaseCandidate>

To verify file integrity after download, you may use the [sha1sum signature for the above image](https://public.f123.org/releaseCandidate.sha1sum).

Please be aware that this is a test image and there may be bugs, some of which could be severe. Please do not use test installations as your primary machine or in production environments.

If you are not an experienced computer user, we recommend that you use instead our [latest stable version of F123Light](https://guide.f123.org/getting-started.html#download), as it has been tested more thoroughly than the above version.

