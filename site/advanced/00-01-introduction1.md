This guide is intended for those who are more experienced (or are not very experienced but want to learn more technically advanced things), about the F123Light operating system. Should you need information about the overall strategy of the project, our motivations, or other non-technical matters, please read our [general project guide](strategy.html).

Should you have suggestions, corrections, new content, new code, or other contributions to this project, please [join one of our e-mail lists](getting-started.html#our-e-mail-lists) and let us know about it.

