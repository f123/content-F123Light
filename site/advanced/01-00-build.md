## Updating F123Light on the Command Line

Advanced users can perform an update from the command line with the command: "update-f123light"

If such a user is helping us test recent changes to the code, he or she can use a special parameter to update from the development branch. To specify the development branch, the correct command would be: "update-f123light -b dev

## Building Your Own F123Light Image

Experienced users also have the option of generating or building their own image. Every copy of F123Light includes the command "build-f123light", which automatically generates the image file, which can then be burned or flashed to a MicroSD card for use. Please make sure your working SD card has at least 8Gb of free space to perform this procedure.

## Burning/Flashing Your New Image Using the Command Line

Experienced or ambitious users can burn the image they have just built on their own Raspberry Pi by using the "sudo fdisk -l" command to identify which device or drive name to use, after plugging-in the USB adapter containing the target MicroSD card.

*Warning*: This is a risky procedure, so it should not be attempted by inexperienced users or those with valuable information on their computers, or who do not have an up-to-date backup of same.

Once the user has identified what device holds his or her operating system, and which device will be used to burn the image, the user can type something like the following to actually flash the image which was just created:

sudo dd if=filename of=device-path bs=2M

The "sudo" string is used for important commands which require admin powers. The "dd" command is also called disk-dump, and it is used to make exact copies of disks or to burn images on same. The "if" and "of" parameters, stand for input file and output file, and the "bs" parameter stands for block size. Specifying block size is optional, but we have found that using "2M" speeds-up the process. Do note that the "M" in this parameter must be capitalized.

## Pre-Configuring Wireless Access (WiFi)

Anyone using our "build-f123light" build script to create an image, can include his or her wireless network information on the configuration file under F123Light/build/ and the computer will boot already with WiFi access to the Internet. However, if you do this, make sure you also change the default passwords for both the normal and root users.

