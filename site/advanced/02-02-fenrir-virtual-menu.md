## Fenrir Virtual Menus

Fenrir has a new feature that will perform key presses for you. This feature is very powerful, because it can provide easy to use shortcuts and menus that work the same across applications.

For example, if you have two editors, Nano and Vim, they save files in different ways. In F123Light, to save a file in Nano, you press control+s. In Vim, however, you must make sure you are in command mode by pressing the escape key, then enter the command :w. If Fenrir has a menu defined for both text editors, you could simply activate the menu by pressing Fenrir+F10 or Fenrir+Alt, then press f for files, and s for save.

You can also use the arrow keys and enter to select menu choices. If you open the menu and change your mind, simply press escape to close it. If you type a letter that does not exist as a menu option, Fenrir will make an error sound.

You can already try virtual menus on the Nano text editor.

