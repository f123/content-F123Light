### Commands

This is the commands class used to build and work with Fenrir commands, i.e. what happens when the user presses a Fenrir key shortcut. The folder structure for building various types of commands is also found here in src/fenrirscreenreader/commands.

* command_template.py
Contains a basic command skeleton that is used for all but onSwitchApplicationProfile commands, last uses switchTrigger_template (as there are load  and unload functions)

#### basic functions:

1.  initialize is called on command creation. It's an own function rather than in custructure.
2.  shutdown is called if the command is reloaded or fenrir gets shutdown (thought to clean up things)
3.  getDescription: gives a description for tutorial mode, or in case of vMenu, it's the entry name
4.  run() is executed if the command is invoked
5.  the callback function is currently unused. It's thought for special cases like in "sayAll," to notify that the current word is spoken and invoke the next one, so custom usage.

* switchTrigger_template.py
These are the currently disabled application profiles.

* commands
The commands folder contains the "normal" commands like review or say time. Just copy the commands_template.py above into it and add your specific command code. To assign the command in this folder to a keyboard binding, just assign the keys to the command filename without .py. example:
Assign add_word_to_spell_check.py to 2 times fenrir + S
```2,KEY_FENRIR,KEY_S=add_word_to_spell_check```
fenrir will read all python files in a command folder and match the filename to the shortcut without .py. The keybindings are found in config/keyboard/.

* help
Contains the automatically assigned navigation for tutorial mode.

* onApplicationChange
All commands in this folder are triggerd in order if the current foreground application changes - currently disabled

* onByteInput
All commands in this folder are triggerd in order if a byte input event was fired

* onCursorChange
All commands in this folder are triggerd in order if the cursor position changes

* onHeartBeat
All commands in this folder are triggerd in order on heartbeat (a timer defined in the Fenrir code).

* onKeyInput
All commands in this folder are triggerd in order if a key input event was fired.

* onPlugInputDevice
All commands in this folder are triggerd in order if a plug input device event was fired.

* onScreenChanged
All commands in this folder are triggerd in order if an on screen change event was fired (i.e. as you switch from TTY3 to TTY5).

* onScreenUpdate
All commands in this folder are triggerd in order if an on screen update event was fired (as new text appears on the current screen).

* onSwitchApplicationProfile
Contains the application profiles. It matches the application by name and calls the oldApplication.unload and newApplication.load functions, currently disabled.

* quickMenu
Contains the NVDA quick menu navigation.

* sayAll
Contains the callbacks for WIP say all.

* vmenu-navigation
Contains the automatically assigned navigation keys for the vMenu.

* vmenu-profiles
Contains the vMenu structure itself.

It is only necessary to place a command in the folder that corresponds to the event where it should be executed. Fenrir will read all commands in the event folder and execute them in numerical order. If it is desirable to execute commands within an event folder in a specific order, place numbers at the beginning of their filenames, i.e. 00-do-something.py and 01-do-something-else.py.

