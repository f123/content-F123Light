# F123Light Modes

in order to help keep clutter down, and to make things more efficient, F123Light offers modes that change the way it presents information. For now, this mostly means different menus, but as this service grows, it could potentially change nearly every aspect of the system.

Only someone with administrative access can change modes, and only by running a command from the command line. To change modes, run the following command:

    sudo /usr/lib/F123-wrappers/change-mode.sh

You will be presented with a menu of all available modes. Just arrow to the one you want and press enter to activate it. If you already know the mode you want, simply specify it at the end of the command above.

The idea behind modes is to have a "default" mode, which only shows the most user-friendly software; a "test" mode, which might be used to administer tests in schools; and "admin" or "advanced" mode, which will give full access for those who are sophisticated users of technology; and other modes such as games, will certainly be suggested by our users.

