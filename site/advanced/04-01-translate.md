# Translating with GetText

If you want to help with F123Light translations, please get in touch with us, as we have simplified greatly the process described here. However, if you just want to translate doing every step manually, read-on.

To create a translation for a project, the first step is to create a .pot file.

    xgettext -o name.pot -L sourceCodeLanguage file(s)

Source language can be one of C, C++, ObjectiveC, PO, Shell, Python, Lisp, EmacsLisp, librep, Scheme, Smalltalk, Java, JavaProperties, C#, awk, YCP, Tcl, Perl, PHP, GCC-source, NXStringTable, RST, Glade, Lua, JavaScript, Vala, or Desktop according to the man page. There used to be a python specific translation script called pygettext3 but it is now deprecated. After the .pot file is created, the next step is to create a .po file that contains the translation. The .po must be specified with the full language code including .UTF-8 or appropriate encoding string.

    msginit -i name.pot -l pt_BR.UTF-8

The package name and copyright information should be set in the .po file. The .po file is the locale name without the .UTF-8, so in the example above, pt_BR.po.

To finally generate the .mo file, which is what actually provides the translation usable by the software, `run the following:

    msgfmt -o name.mo pt_BR.po

For the .mo file to actually be used, it must be placed in the correct directory to be available to gettext. Move the .mo file as in:

mv name.mo /usr/share/locale/pt_BR/LC_MESSAGES/name.mo


