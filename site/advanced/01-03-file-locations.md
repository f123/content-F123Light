## Important locations and File Structure

With the exceptions of update-f123light, build-f123light, and first-boot, the scripts that power F123Light reside in the /usr/lib/F123-wrappers and /usr/lib/F123-includes directories.

Each file is named according to what it does, and there is a brief description at the top of the file with the copyright notice. The files in /usr/lib/F123-wrappers are the end product that present information to the user. The files in /usr/lib/F123-includes are functions and code that gets reused almost everywhere to make things easier to extend.

So, to have menus and input boxes, etc, similar to what we use, you can include something like the following in your scripts:

    # Load F123 includes
    if [[ -d /usr/lib/F123-includes/ ]]; then
    for i in /usr/lib/F123-includes/*.sh ; do
            source $i
    done
    fi

If you need to interact with the preferences, which include such things as tip display options, preferred browser, email client, etc, you can get access to it by sourcing the ~/.preferences file like so:

    source ~/.preferences

Preferences are stored in an associative array. In order to keep the preferences file well formatted and readable, when writing new preferences please use the write_preferences function defined in  /usr/lib/F123-includes/preferences-manager.sh.

F123Light specific overwrites for applications are in /etc/F123-Config/. This allows different behavior for applications such as irssi when called from the help menu as apposed to when it is called by the user specifically. Users will be able to customize their day to day copy of irssi as much as they want, and the chat support option from the menu will still work as designed when needed.

Also in /etc/F123-Config/ is a subdirectory called backup. This contains a copy of speechd.conf and any other files that may be necessary to reset something vital to a working state.

