## Creating a Virtual menu in Fenrir

Since F123Light uses many console applications with non-standard keybindings, the virtual menu is a Fenrir add on which will translate more conventional key combinations into those unusual ones, so the end-user does not have to worry about learning entirely new keyboard shortcuts.

Fenrir is a flexible framework to create screenreaders for the commandline. It is entirely plugin driven. The virtual menu is no different, as it uses the same simple structure. Its a class named command() with 3 funktions:

1. initialize(environment)
Is invoked by fenrir while loading. The Parameter „environment“ is the complete fenrir runtime, drivers, managers, settings and all kinds of data. Its like the engine in an car. This is the most complex part. Anybody interested in additional details in this area should ask about it in: <mumble.linux-a11y.org> or <irc.linux-a11y.org>.

2. getDescription() returns a text  spoken in tutorial mode or is used as menu entry name.

3. run, this is fired on activation. It can access all kind of information and functionality with environment.

Especially for virtual menu, we have created a key sequencer that uses evdev key names with states. A special sequence name with the name sleep gives a small break.

The menu by itself is structured by folders. With KEY (for key devices) and BYTE for terminal emulation (using escape sequences).
Above those folders is a folder per application, as shown below. The user can be locked in those applications menus using the remote manager so they will not get confused by wrong entrys)

For F123Light, the code for virtual menus resides in:
/usr/share/fenrirscreenreader/menus/KEY/

Inside this directory  is the actual macro structure. The Directories directly under KEY are application names that contain the menu for the application.

    % ls
    vim/  nano/  mutt/  mc/

Inside those directories  are the actual virtual menu directories, so "file", "Tools", etc.

Here, from the File directory for Nano, is the save command. This activates control+S which is the Fenrir command for Nano. Description is the help text for it if you are trying to learn the keys.

    # Code

    #!/usr/bin/env python
    # -*- encoding: utf-8
    from fenrirscreenreader.core import debug


    class command():
        def __init__(self):
            pass

        def initialize(self, environment):
            self.env = environment
            self.keyMakro = [[1, 'KEY_LEFTCTRL'],
                             [1, 'KEY_S'],
                             [0.05, 'SLEEP'],
                             [0, 'KEY_S'],
                             [0, 'KEY_LEFTCTRL']]

        def shutdown(self):
            pass

        def getDescription(self):
            return "Save your work."

        def run(self):
            self.env['runtime']['outputManager'].presentText(
                "Okay, you will now be asked to save your work.", interrupt=True)
            if self.env['runtime']['inputManager'].getShortcutType() in ['KEY']:
                self.env['runtime']['inputManager'].sendKeys(self.keyMakro)
            elif self.env['runtime']['inputManager'].getShortcutType() in ['BYTE']:
                self.env['runtime']['byteManager'].sendBytes(self.byteMakro)

        def setCallback(self, callback):
            pass

    # End Code

