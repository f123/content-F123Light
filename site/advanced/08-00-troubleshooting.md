# Troubleshooting Problems

In this section we have suggestions regarding how you may investigate problems with your software. The idea is to help you figure things out on your own, or at the very least, help you find information which might allow others to help you more rapidly.

