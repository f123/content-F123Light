## Build a Custom Image

To build a new image, you will need to make sure you have at least 8GB of free space. You may need aditional space for the work directory. If you try to build an image with out enough free space the script will alert you to the issue and stop.

You can customize your image by passing special parameters to it. Here is a list of things you can customize about your image: 

* -d <directory> or --work-directory <directory>: The working directory where the image is created and mounted and packages are cached. The default is the current directory.
* -o <file> or --output-file <file>: the name of the image file. The default is F123Light-yy.mm.dd.img where yy.mm..dd is the 2 digit year, 2digit month, and 2 digit day.
* -H <name> or --hostname <name>: The host name of the installed OS. The default is f123light.
* -r <password> or --root-password <password>: The root user's password. The default is root.
* -u <name> or --user=<name>: The name of the regular non-root user. The default is f123.
* -p <password> or --password=<password>: the regular user's password. The default is f123.
* -n <SSID> or --network-name <SSID>: The name of a preconfigured wifi network access point.
* -w <password> or --wifi-password <password>: A preconfigured wifi network password. Requires --network-name. Remember to inclose the password in 'apostrophees or single quotes' if it contains special characters or spaces.

Here are a couple of examples of building an image with some parameters given on the command line: 

    build-f123light --output-file custom.img

This will make an image called custom.img.

    build-f123light -n wifi-network-name -w 'your-password'

This example creates an image which already contains your Wifi login, so you will not have to re-enter your WiFi password at boot time. Just like the two examples above, you can enter all the parameters listed above and the image will be built with all that information already included.

