# Customizing the F123Light Menus

You may want to add your favorite application to the menu system for easy launching. To do this, create a file called menu under ~/.config/F123 like this:

    mkdir -p ~/.config/F123
    nano ~/.config/F123/menu

The process for adding a command line based application is pretty straight forward. This, for example, will add vim:

    exec:Vim Text Editor::vim

Some applications require special directives to pdmenu, those that do not stay open for example, may need the pause flag. For more information, please read:

    man pdmenurc 

Adding graphical applications, such as chromium takes a bit more work. To do this, we have to suspend Fenrir so that it and Orca aren't trying to read at the same time. We then have to call the graphical application with a special script that loads all the X session components and starts the Orca screen reader. Here is an example to add lxterminal to your personal menu:  

    group:LXTerminal
    exec:::python /usr/share/fenrirscreenreader/tools/fenrir-ignore-screen &> /dev/null
    exec:::echo -n "setting set screen#suspendingScreen=$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
    exec:::startx /usr/lib/F123-wrappers/xlauncher lxterminal
    exec:::python /usr/share/fenrirscreenreader/tools/fenrir-unignore-screen &> /dev/null
    exec:::echo -n "setting set screen#suspendingScreen=$(</tmp/fenrirSuspend)" | socat - UNIX-CLIENT:/tmp/fenrirscreenreader-deamon.sock
    endgroup

Notice that there are some execs even after the lxterminal application is called. This is to restore Fenrir so it will begin reading again after the X session closes.

### Finding Binary Application Files

You will usually find binary application files at: /usr/bin

If you are ever unsure though, the following command will show you where it is, assuming it is in your executable path:  

    command -v application-name

For example:   

    % command -v firefox
    /usr/bin/firefox

Please note that the "$" symbol is usually used to indicate the command line prompt. In F123Light, we use the "%" symbol, but this can be easily changed.

