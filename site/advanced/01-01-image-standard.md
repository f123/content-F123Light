## Build a Standard Image with F123Light Itself

To build a new image, you will need to make sure you have at least 8GB of free space. You may need aditional space for the work directory. If you try to build an image with out enough free space the script will alert you to the issue and stop.

To build an image with all default parameters, go to the command line and enter the following:

    build-f123light

