### Core Classes

These core classes define the heart of Fenrir, found in src/fenrirscreenreader/core.

* applicationManager.py
Currently unused. It was initially used to detect the currently running application to automatically load application profiles, but no reliable detection method was found for applications running inside of screen or tmux.

* attributeManager.py
This is currently the API to handle all kinds of attribute stuff. The hilight tracking is also analyzed here, i.e.
```attributeManager.getAttributeByXY(0,0)```
gives the attributes for the first cell and row (starting at the upper left corner on the screen). An attribute set looks like the following list:

```  #start attribute
[
    'default', # fg
    'default', # bg
    False, # bold
    False, # italics
    False, # underscore
    False, # strikethrough
    False, # reverse
    False, # blink
    'default', # fontsize
    'default' # fontfamily
]
  #end attribute```

Attribute[2] indicates whether or not the cell is bold.

* barrierManager.py
Manages the barrier feature. It chains the current window down to the configured barriers.

* brailleDriver.py
Is the base class for all brailleDrivers.

* byteManager.py
Handles byte input used in terminal emulation mode. It's the opposite of inputManager, which consumes key press/ release events.

* commandData.py
This is a container to hold command information. It will be deprecated in the future.

* commandManager.py
loa

- cursorManager.py
Holds all cursor related information and handles the review cursor.

- debug.py
Contains the debug states (Info, warning, error)

* debugManager.py
produces the debug output and writes it to a file or prints it on screen.

- environment.py
The environment contains almost everything fenrir is, and it's passed to every object fenrir loads, to have information available everywhere. This makes it quite easy to create plugins, as the commands get the information as well. It's a python dictionary. It's mostly named self.env. The only place where it is named environment is the fenrirManager class (for historical reasons). The environment looks like this:

```environment = {
'screen': screenData, #  contains the screen information, better access it using the API in cursorManager and screenManager. This will be deprecated.
'runtime': runtimeData, # contains all in "core/*Manager.py" classes. its the point to communicate with the fenrir core. its very important example below.
'general': generalData, # contains the general information, better access it using the API in fenrirManager will eventually be deprecated
'settings': settingsData, # contains the settings information, better access it using the API in settingsManager. this is not going to be deprecated
'commandInfo': commandData.commandInfo, # contains global command info like last execution times, itwill be removed, as it was not used.
'commandBuffer': commandData.commandBuffer, # contains information that is needed to sored persistent, like the begin and end mark. for this we have the memoryManager now. this is going to be deprecated in fenrir 3
'input': inputData, # contains the input information, better access it using the API in inputManager. This will be deprecated.
'punctuation': punctuationData, # contains the punctuation information, better access it using the API in punctuationManager This will be deprecated.
'output': outputData,  # contains the output information, better access it using the API in outputManager This will be deprecated.
'soundIcons': {}, # contains the soundicons
'bindings': {}, # contains the key bindings
'commands':{} # contains all the commands loaded by the commandManager
}```

example of environ usage:
```self.env['runtime']['outputManager'].presentText('hello world' , soundIcon='', interrupt=True)```
runtime contains all managers (without the .py) as dict entries, so the functions could be used there. The output manager has the method "presentText" that is called here

Get the setting dateFormat in section general using the settings manager:
```dateFormat = self.env['runtime']['settingsManager'].getSetting('general', 'dateFormat')```

getSetting returns text. There are others to cast as bool or int. Here is an exemple for bool:
```isAutoReadIncomming = self.env['runtime']['settingsManager'].getSettingAsBool('speech', 'autoReadIncoming')```

Settings can also be set here. The following disables "autoReadIncomming":
```self.env['runtime']['settingsManager'].setSetting('speech', 'autoReadIncoming', str(False))```

* eventData.py
An enum with all event types

* eventManager.py
This is the queue for events. It has some methods to add events to the queue and dispatches the events to the right places. Most of them are passed to the fenrirManager.

* fenrirManager.py
This is used for startup. It ties all the event cases together where they are passed to the right managers. It also checks for shortcuts and consumes the input, i.e.
```input event -> eventManager -> fenrirManager -> inputManager
screen change event -> eventManager -> fenrirManager -> screenManager```

* generalData.py
This is a container to hold general information. It will be deprecated in the future.

* helpManager.py
Creates the tutorial information and controls the navigation in tutorial mode

* i18n.py
Loads the gettext and translation module.

- inputData.py
This is a container to hold input information. It will be deprecated in the future.

* inputDriver.py
This is the base class for all input drivers

* inputManager.py
Gets key input events like "key a pressed" and "key a released" and tracks the current pressed keys.
event chain:
input driver (PlugInputDevice) -> eventManager -> fenrirManager -> inputManager
input driver (KeyboardInput) -> eventManager -> fenrirManager -> inputManager

* memoryManager.py
Exists to manage persistent memory like the clipboard.

* outputData.py
This is a container to hold output information. It will be deprecated in the future.

* outputManager.py
This contains methods for playing sound icons and speaking.

* processManager.py
This spawns the threads needed for the drivers to progress more quickly. Fenrir is multithreaded and multiprocessed. so it can split out drivers workers to own processes to get multi cpu core power.

* punctuationData.py
This is a container to hold punctuation information. It will be deprecated in the future.

* punctuationManager.py
Prepares the punctuation for output, replaces custom dicts and removes what will not be spoken.

* quickMenuManager.py
Manages the NVDA like arrow menu for speech rate, volume and pitch.

* remoteDriver.py
The base class for remote drivers.

* remoteManager.py
Loads the remote drivers. Accepts and proceeds all incomming commands.
event chain:
remote driver (RemoteIncomming) -> eventManager -> fenrirManager -> remoteManager

* runtimeData.py
This is just a placeholder an will be removed.

* sayAllManager.py
This is a WIP for the say all functionality

* screenData.py
This is a container to hold screen information. It will be deprecated in the future.

* screenDriver.py
This is the base class for all screen drivers.

* screenManager.py
Creates the diff and tracks the cursor.
event chain:
screen driver (ScreenUpdate) -> eventManager -> fenrirManager -> screenManager
screen driver (ScreenChanged) -> eventManager -> fenrirManager -> screenManager

* settingsData.py
Holds the default settings.

* settingsManager.py
The settings manager contains all methods to load, save and querry settings.

* soundDriver.py
Base class for sound drivers.

* speechDriver.py
Base class for speech drivers.

* tableManager.py
WIP for a table cell based review. It should speak the cellname in review mode. Example
ps
```PID TTY
123 TTY1```
it should not navigate:
PID, TTY, 123, TTY1
but
PID: 123, TTY: TTY1
to keep track of the current cell

* textManager.py
Used to manipulate the output for speech, i.e.
##########
is transformed to:
10 times #

* vmenuManager.py
Rebinds the vMenu keys, loads the vMenu, contains the navigation for vMenu, etc.

most of the *Data.py should be moved to the managers in future.

