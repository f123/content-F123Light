### Folder Structure

Fenrir's source code is organized in the git tree as follows:

* src/
  This is the top level of the source code, and contains the executables:
  + fenrir
    run fenrir in foreground (useful for testing or terminal emulation)
  + fenrir-pypy
    run fenrir in foreground using the faster pypy python JIT compiler (also useful for testing or terminal emulation)
  + fenrir-daemon
    spawns fenrir as daemon (used for the "global" background service to read the TTYs).
  + fenrir-daemon-pypy
    spawns fenrir as daemon using the faster pypy python JIT compiler which gives better performance (also used for the "global" background service to read the TTYs).

  + src/fenrirscreenreader/
    The top level of the core and modular Fenrir code
    - brailleDriver
      Contains all braille drivers, currently incomplete. brailleDriver modules are loaded and accessed in core/outputManager.py. All files here are inherited from core/brailleDriver.py.
    - commands:
      Contains a substructure for all commands, vMenus and triggers.
    - core
      Contains all the core classes. Its the fenrir API itself. Holds the settings and loads the drivers.
    - inputDriver
      Contains all input drivers. inputDriver modules are loaded and accessed in core/inputManager.py. All files here are inherited from core/inputDriver.py
    - remoteDriver
      Contains all remote drivers that give access using the remote subsystem. remoteDriver modules are loaded and accessed in core/remoteManager.py. All files here are inherited from core/remoteDriver.py
    - screenDriver
      Contains the screen drivers that collect information about the screen and its contents. screenDriver modules are loaded and accessed in core/screenManager.py. All files here are inherited from core/screenDriver.py
    - soundDriver
      Contains the sound drivers for the soundicons. soundDriver modules are loaded and accessed in core/outputManager.py. All files here are inherited from core/soundDriver.py
    - speechDriver
      Contains the text to speech engine drivers. speechDriver modules are loaded and accessed in core/outputManager.py. All files here are inherited from core/speechDriver.py
    - utils
      Contains usefull functions for word wrapping and more.

