## Selecting Fenrir Screen Reader Branches

Sometimes you may want to try a new feature in Fenrir that has not made it to the stable release. For this, you can exit to the command line, then run the following command: 

    select-fenrir

You will be presented with a list of branches in the git repository. Simply select the branch you want a and press enter. The current version of Fenrir will shut down and the version from the selected branch will start.

There are safeguards in place to make sure you do not end up with a broken Fenrir. If you hear Fenrir speaking, you can press enter to use the selected version. If you do not press enter with in 30 seconds, the computer will restart and you will be using the stable, installed version.

You can get back to your installed copy of Fenrir by selecting "stable" from the list of branches or by restarting your computer. Also, if you already know the branch you want, you can specify it on the command line and skip the menu system altogether. For example, type the following to launch Fenrir from the master branch:

    select-fenrir master

