## Fenrir Resources and Contacting Developers

The best places to find Fenrir contributors are:  

* Voice chat: <mumble.linux-a11y.org>
* IRC: <irc.linux-a11y.org>, Room: #a11y
* Web site: <https://linux-a11y.org>
* E-mail list: <fenrir-screenreader@freelists.org>
* Wiki: <https://wiki.linux-a11y.org/doku.php?id=fenrir>
* Source Code: <https://github.com/chrys87/fenrir>

