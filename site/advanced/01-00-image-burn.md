## Building Your Own F123Light Image

Experienced users have the option of generating or building their own image. Every copy of F123Light includes the command "build-f123light", which automatically generates the image file, which can then be burned or flashed to a MicroSD card for use.

To build a new image, you will need to make sure you have at least 8GB of free space. You may need aditional space for the work directory. If you try to build an image with out enough free space the script will alert you to the issue and stop.

To build an image with all default parameters, go to the command line and enter the following:

    build-f123light

## Burning/Flashing Your New Image Using the Command Line

Experienced or ambitious users can burn the image they have just built on their own Raspberry Pi by using the "sudo fdisk -l" command to identify which device or drive name to use, after plugging-in the USB adapter containing the target MicroSD card.

*Warning*: This is a risky procedure, so it should not be attempted by inexperienced users or those with valuable information on their computers, or who do not have an up-to-date backup of same.

Once the user has identified what device holds his or her operating system, and which device will be used to burn the image, the user can type something like the following to actually flash the image which was just created:

sudo dd if=filename of=device-path bs=2M

The "sudo" string is used for important commands which require admin powers. The "dd" command is also called disk-dump, and it is used to make exact copies of disks or to burn images on same. The "if" and "of" parameters, stand for input file and output file, and the "bs" parameter stands for block size. Specifying block size is optional, but we have found that using "2M" speeds-up the process. Do note that the "M" in this parameter must be capitalized.

