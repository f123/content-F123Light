## Troubleshooting update-f123light

### Update Problems with Version 19.02.04 of F123Light

For some users, it was necessary to run the update option under the 'Settings' menu, twice, and then their version got correctly updated. If that does not solve your problem, you might want to research further.

### Logs and More

You will probably find helpful details in this log:

    /var/log/update-f123light.log

You might also want to try this command for more potentially useful information:

    pacman -Qi update-f123light-git

Finally, this command might help with updates, and if there are none, then that is good to know as well:

    yay -Syu

