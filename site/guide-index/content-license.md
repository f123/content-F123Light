---

This work by [F123 Consulting](https://f123.org/en/) is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/). We also have another way for you to freely use our content, if this license does not meet your needs. [Contact us for alternative licensing options](mailto:license@f123.org).

