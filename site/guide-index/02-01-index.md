Feel free to suggest additional resources for this page. Here is what we already have:  

* [The Challenges We Will Overcome and How](strategy.html)
* [Practical Guidance on How to Download and Use F123Light](getting-started.html)
* [Advanced Technical Information](advanced.html)
* [Different Ways in Which You Can Help](https://guide.f123.org/getting-started.html#contributing-time-talent-contacts-etc)
* [Our E-mail Lists for Announcements, Questions, and Suggestions](https://public.f123.org/guide/getting-started.html#our-e-mail-lists)

## Computer-Building Workshops

If you would like to be part of one such workshop, please join one of our lists and [watch and like this video](https://youtu.be/KBM8Nq-f2T4), so we can win the Holman Prize. Thanks!

## Social Media and Lists

We generally do not follow nor respond to social media, so if you need to get in touch with us, the best way is [our lists](https://guide.f123.org/getting-started.html#our-e-mail-lists). If it is something you rather write directly to us, here are [our e-mail addresses according to language](https://public.f123.org/guide/getting-started.html#contacting-our-team-directly).

