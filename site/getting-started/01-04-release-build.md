# Developers and Other Advanced Users

## Updating F123Light on the Command Line

Advanced users can perform an update from the command line with the command: "update-f123light"

If such a user is helping us test recent changes to the code, he or she can use a special parameter to update from the development branch. To specify the development branch, the correct command would be: "update-f123light -b dev

## Building Your Own F123Light Image

Experienced users also have the option of generating or building their own image. Every copy of F123Light includes the command "build-f123light", which automatically generates the image file, which can then be burned or flashed to a MicroSD card for use. Please make sure your working SD card has at least 8Gb of free space to perform this procedure. You may lern more about building your own image in our [resources for advanced users page](advanced.html).

