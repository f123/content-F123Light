### Selecting the Operating System Language

The very first version of F123Light is available in English, but shortly will also be available in Brazilian Portuguese, Spanish, and Arabic. Please [join the e-mail list corresponding to the language you are interested in](guide.html#our-e-mail-lists), and you will be notified when it becomes available.

The very first time F123Light loads, it gives you a chance to select the language you will use the system in. Press 'Enter' for English, or use the up and down arrow keys to select another option and press 'Enter'. Once you do this, the system will make a sound and restart in the language you selected.

If you selected a language which is still not officially supported, the interface might still not be entirely translated.

