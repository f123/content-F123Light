## Acknowledgements

Our work is only possible thanks to a [wide variety of persons and organizations](https://f123.org/en/acknowledgements), from volunteers and our own users, to developers, translators, journalists, teachers, funders, and many more. Thank you all!

