## An Introduction to the Text Editor

The default text editor in the F123Light system is called Nice Editor (NE). This editor is small in size, fast, easy to use, and feature rich.

The keyboard shortcuts or key bindings used in the F123Light version of Nice Editor are modified so that anybody who has used an editor under Windows or other graphical user interfaces (GUIS), will have a very easy time using it.

You will also notice that for some functions, more than one shortcut is available. This is because depending on what editor you use the most, you might for example, close a document with Ctrl+W or maybe with Ctrl+Q. Since both combinations work on F123Light's version of Nano, you will probably be able to just keep using what you are used-to.

Here is a list of shortcuts. Remember, you can also use Fenrir's virtual menu system (Insert+F10), if you get stuck or can't remember something: 

* Control+O: Open a new file. This does not close any previous files you have open.
* Control+S: Save current file.
* Control+w: Save the current file and exit if there are no other open files.
* Control+q: Save the current file and exit if there are no other open files.
* F4: Save the current file and exit if there are no other open files.
* F7: Activates the spellchecker.
* Hold down the Shift key and move with the arrow keys to highlight portions of the text and use Ctrl+X, Ctrl+C, and Ctrl+V to cut, copy, and paste.
* Press Home or End to move directly to the beginning or end of the current line.
* Press Control and left or right arrow to move one word at a time.
* Press Ctrl+Home or Ctrl+End to move directly to the beginning or end of the current file.
* Control+x: Cut the current line so you can use Control+V to move it, i.e. paste it, elsewhere. (only available inside Nano)
* Control+c: Copy the current line so you can use Control+V to paste it elsewhere. (only available inside Nano)
* Control+v: Paste the current clipboard to the file. (only available inside Nano)
* Alt+Space: Changes to the next file, if you have more than one open.
* Control+Space: Changes to the previous file, if you have more than one open.
* Control+f: Find text within the current document.
* Control+h: Find and replace text in the current document.
* Alt+c: Try to complete the current word. For this to work, the word you want to complete must already exist somewhere in the document.
* Alt+m: Get character, line, and word count for the current document.

Please note that the Nice Editor sometimes calls files "buffers". You can consider buffers the exact same thing as a file. The only reason this term is used, is because it refers to information that resides on memory and might not be already saved to disk.

If you forget any of these commands, you can always find this text inside the Virtual Menu (Insert+F10), under the "Help" menu, under "About Nice Editor".

