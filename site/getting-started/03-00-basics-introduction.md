# Introduction to the Basics

In this section we will describe the commands and shortcuts you can use for routine operations with F123Light. Since the system is undergoing rapid development, there may be times in which the documentation does not perfectly match the latest version of the software. If you encounter difficulties, please [let us know through one of our e-mail lists](guide.html#our-e-mail-lists).

