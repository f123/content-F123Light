# Latest Release

## Download

This release is still Alpha code, which means that it is under development and it has important problems and limitations.

We have a single installation file for all available languages, but not all translations are equally complete. Much work remains to be done and any help is most welcome.

You can find the compressed image file here:
<https://public.f123.org/latestImage>

 We are also including the [sha1sum signature for the image](latestImage.sha1sum) file, so you can verify file integrity after download. Please note that the image is compressed with the xz format, which is more efficient than the zip format. You may find [information on how to uncompress xz files here](http://www.e7z.org/open-xz-txz.htm).

### The First Time You Turn-On the Computer

During the first time you start or boot the computer, the software will auto-expand the partition and file system to match the size of the Micro-SD card, so it can take a minute and a half or more for the Raspberry Pi 3B+ computer to be ready. Subsequent boots will be faster, i.e. around 30 seconds.

### Default Username and Passwords

the default username is: f123

The default user and admin password is lowercase: f123

The hostname is (lowercase): f123light

Advanced users can easily build images with WiFi and other passwords already set to personalized values. [Information on custom images can be found here](advanced.html#build-a-custom-image).

