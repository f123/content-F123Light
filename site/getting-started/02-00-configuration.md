## Initial Operating System Configuration

The very first time you start-up your computer with the MicroSD card containing F123Light, the start-up or boot process will take longer than usual, as the system is uncompressing and preparing the file system for use. The larger the capacity in your MicroSD card (i.e. the more memory it has), the longer this process will take. In any case, it will generally be two or three minutes at the most.

