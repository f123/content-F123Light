# Bluetooth in F123Light

Connecting bluetooth devices such as headsets and keyboards is fairly straightforward in F123Light.

## Common Steps to Get Started

* Start by navigating to the Settings menu by pressing s.
* Press b to open the bluetooth manager.
* You will hear "screen reader on" followed by "Bluetooth devices frame." Note: this may take up to 15 seconds.
* Put your device into  pairing or discovery mode. Each device can be different, so it helps to know how to do this on the specific device. For example, some devices allow you to hold in the power button, while others have dedicated pairing buttons.
* The "Search button" in the bluetooth device manager is already focused. Press the space bar to activate it.
* Press the tab key twice. You will hear some table information that you can ignore.
* Use the up and down arrow keys to navigate to the device you want to pair.

## Pairing Headsets and Speakers

* Press the tab key to leave the list of devices and focus the search button.
* Press the right arrow key twice. The first time you will hear "trust push button." The next time you will hear "Setup ... push button."
* Press the space bar to activate setup.
* The pairing method is already set, so you can press shift+tab to the "Next push button."
* "Audio sink"  is selected by default, so again press shift+tab to the "Next push button."
* Your headset or speakers should now be connected. Press the space bar to close the setup application.
* Press the left arrow once to "Trust push button" and press the space bar.
* If you want to pair another device, put the new device into pairing mode, press the left arrow again to the "Search push button" and then tab
 to go back to the list and find the new device. Repeat either the pairing steps for headsets or speakers, or if your device is a keyboard, continue to the next heading.

## Pairing a keyboard

* Press the tab key to leave the list and press the right arrow to the "Pair push button."
* Press the space bar on your USB keyboard to begin the pairing process.
* You will hear "pairing request for" followed by a string of letters and numbers, followed by a short pause and a 6-digit pairing code, all numbers. On the bluetooth keyboard, type in the 6-digit code followed by the enter key.
* Be sure to listen for all 6 digits of the code; it is spoken as a whole number. However, speech is interrupted as you type on the bluetooth keyboard, so you can listen for the code again if you get stuck in the middle.
* You should hear "Authorization request for" followed by the letters and numbers you heard when pairing. Press the tab key on the USB keyboard to accept the request.
* Once you accept the authorization request, you should hear an upward sweeping tone. This indicates that your keyboard is connected.
* Press the escape key until you hear "Bluetooth Devices frame" again. Press the left arrow to the "Trust push button" and press the space bar.

## All Devices Are Paired and Connected

* Press alt+F4 to return to the settings menu and the escape key to return to the F123Light main menu.

