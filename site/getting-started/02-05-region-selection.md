### Selecting Your Region

The F123Light operating system sometimes needs to know where in the world you are located. This is helpful so it can find out what day and time it is, and it can then figure-out if, for example, a new update to the software is actually new.

First you must use the up and down arrow keys to select the region where you are and then press 'Enter'.

Then you need to select the city where you live from a list and again press 'Enter'. Since the list only contains the largest cities, your own city might not be on the list. If this happens, please select another city which is near-by, or at least in the same timezone.

