### Change System Passwords

Another question that comes-up when you boot your computer for the first time, relates to changing system passwords. If you are just testing the system and have no plans to use it for more than a few minutes, then there is no nee for you to change system passwords. However, if you will use the system for more than just a few minutes, we recommend that you change the default passwords.

If you press "Enter" for "Yes", you will have a chance to change both the password for the normal user, and the administrator's password. Here is some information which might come-in handy:

The name of the default user in F123Light is: f123

Please note that this name is with a lowercase "f".

The default password for the normal user is exactly the same: f123

The default name of the root or administrator, is: root

Here also, the name is entirely in lowercase.

The default password for the administrator or root user is exactly the same, also in lowercase: root

