# Finding or Offering Help

## Contributing Time, Talent, Contacts, Etc

If you value the work we do, please help us out. You can help in many ways. here are some ideas:  

* [Watch and like this video](https://youtu.be/KBM8Nq-f2T4) so we can win the Holman Prize and offer computer-building workshops in over 12 countries.
* Tell a blind friend or blindness-related organization about us, this is our main page for F123Light: <https://guide.F123.org/>
* [Donate a couple of hours a week as a volunteer](https://public.f123.org/F123-Seeks-Volunteers.html), [helping us with translations](https://crwd.in/f123light), proposal-writing, or marketing efforts.
* Help with [software development](advanced.html)
* [Buy the parts](https://f123.org/partes-f123light) or donate used equipment, and help a blind kid build his or her own talking computer.
* Buy a talking computer and give it to a blind kid.
* [Introduce us](mailto:information@f123.org?subject=Introducing F123) to a wealthy individual, foundation, or government agency that wants to assist the blind in their education, social inclusion, or employment prospects. Even though we prefer e-mail and dislike all types of social networks, if necessary and unavoidable, you can [introduce us using LinkedIn](https://br.linkedin.com/pub/fernando-botelho/5/a4a/6a2).
* [Sponsor one of our courses or computer-building workshops](mailto:information@f123.org?subject=Sponsor F123).

## Our E-mail Lists

To join any of the following e-mail lists, you can send a blank e-mail message to the indicated address. You will then receive an e-mail message from the list, requesting confirmation. Just responding to that confirmation e-mail, i.e. pressing "reply" on your e-mail software, and sending the message without writing anything wil confirm and subscribe you to the list.

If all you want is to get an occasional e-mail from us, sharing the latest news, [join our announcement list](mailto:f123light+subscribe@groups.io). The plan is to send just one message per month, maybe even less.

* Technically inclined users who can communicate in English should subscribe e-mailing [F123e+subscribe@groups.io](mailto:F123e+subscribe@groups.io)
* English speakers who are not developers or technically inclined, but are interested in the project for day-to-day use in note-taking, office work, project management, and other activities, subscribe by sending an e-mail to [F123-Visual-English+subscribe@groups.io](mailto:F123-Visual-English+subscribe@groups.io)
* Non-technical users who speak Spanish can join our group by sending an e-mail message to [F123-Visual-espanol+subscribe@groups.io](mailto:F123-Visual-espanol+subscribe@groups.io)
* Non-technical users who speak Portuguese can join by sending an e-mail message to [F123-Visual-portugues+subscribe@groups.io](mailto:F123-Visual-portugues+subscribe@groups.io)

Those who want to keep in touch with the project through social networks, should know that we rarely respond to posts on social networks. Ideally, please get in touch with us via the above lists or via direct e-mail.

## Contacting Our Team Directly

Some messages are best sent directly to our team. E-mail [messages in English can be sent to this address](mailto:information@f123.org), [in Spanish to this address](mailto:informacion@f123.org), and [in Portuguese to this address](mailto:info@f123.org).

