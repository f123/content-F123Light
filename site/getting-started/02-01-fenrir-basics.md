### Basic Commands for the Fenrir Screen Reader

Most users will be very happy to know that for maybe the first time in history, using a new screen reader will not necessarily require you to learn new keyboard commands. This is because, if you already use the NVDA screen reader, you will probably feel right at home with the key combinations used by Fenrir, the main screen reader of F123Light.

#### Controls for Speech Volume, Rate, and Pitch

Just like in NVDA, you can use Insert, Control, and the arrow keys to modify speech parameters like volume, rate, and pitch. Insert, also called the Fenrir key or the NVDA key, depending on which manual you are reading, can also be the CapsLock key, if you are using the screen reader in laptop mode.

To change speech volume, rate, or pitch, hold down the Fenrir key and the Control key, and use the left and right arrow keys to select if you want to change volume, rate, or pitch. Once you are on the parameter you wish to change, use the up and down arrow keys to increase or decrease that parameter.

Once the speech volume, rate, or pitch is at the level you desire, simply release the Fenrir and the Control keys, and continue using your computer normally.

#### Using Tutorial, Help, or Learn Mode

Pressing the Fenrir and F1 keys, your screen reader will activate its "Tutorial Mode", which allows you to press just about any key, and hear its name and function, if it has a special screen reader function. When you are in "Tutorial Mode", you can press keys and, other than telling you what they normally do, the system will not do anything else. It is an excellent way to learn your way around Fenrir and your keyboard.

To deactivate "Tutorial Mode", simply press Fenrir and F1 again, or press the 'Escape' key, also often labeled as 'ESC'. The 'Escape' key is found usually at the top left of most keyboards.

#### Reviewing the Screen

Just like with most screen readers, Fenrir uses the numeric keypad on the full desktop keyboard for many of its commands. This is especially true for those which involve movement, such as reading previous or next line, word, or character.

Assuming that you are using a full-size desktop keyboard, which has a numeric keypad to its right, you can use: numbers 7, 8, and 9, to read previous, current, and next line; numbers 4, 5, and 6, to read previous, current, and next word; and numbers 1, 2, and 3, to read previous, current, and next character.

If you are using Fenrir in laptop mode, in other words, assuming you do not have a numeric keypad on your keyboard, then you must hold down the CapsLock key, which becomes the new Fenrir key, and press letters U, I, and O, for previous, current, and next line; letters J, K, and L, for previous, current, and next word; and N, M, and the comma symbol, for previous, current, and next character.

Of course, if you used our fast language switching command to change language, the keyboard layout as well as the instant spell checker, will also change. However, the location remains the same. You will still use your right hand in its standard positioning on the keyboard, to read around the screen when holding down the CapsLock key.

