### Configuring WiFi

The quickest way to get internet access is usually to just plug-in an Ethernet cable connected to a router providing that access. However, most often people end-up using wireless access through WiFi.

The configuration process which helps you setup your system when you turn it on for the first time, will also help you configure WiFi, since this access will be required when it is time to select the region where you live.

Setting-up WiFi is easy, just use the up and down arrow keys to select the name of your WiFi network from the provided list and press 'Enter'.

Next, enter the password for that WiFi network or access point 
