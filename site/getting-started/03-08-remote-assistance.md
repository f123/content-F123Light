## Getting Remote Assistance

If you are ever stuck, or have broken something and cannot figure out how to fix it, one of our team members may be able to help you. If you go to the help menu and select "Request remote assistance with my computer", a request for help will be issued to the F123 team.

To get help simply write a brief description of the problem you have. To send the request, type . on a line by itself.

Please note that this service is experimental at this point and is not yet available 24 hours a day, and for now, it is only available in English. In the near future we will share more details on time and language options in our lists and blog.

### Important Notice on Remote Assistance

Requesting remote assistance allows a team member full access to your computer. Anything the root user can do is available to the support staff who answers your request for assistance. You can request a log of everything done in the help session.

