## Burning/Flashing the F123Light Image File to the Micro-SD Card

Once you download and verify the integrity of the compressed version of the image file, you must uncompress this file and flash or burn it to the micro-SD card which you will use on the Raspberry Pi computer. We recommend that you use a micro-SD card of at least 16Gb in size, but an 8Gb card should also work, even though little disk space will be available for your own files.

The expressions "flash" or "burn the image" is used because this is not the same as a copy and paste file transfer. While there are many ways to "flash" the image to the card, we have heard good things about both [Etcher](https://etcher.io/) and [Rufus](https://rufus.ie/) software. Etcher apparently works well in Windows, Mac, and Linux, whereas Rufus works well on Windows.

It is also worth mentioning that burning an image will not prevent you from reformatting or burning other images on the SD card in the future. In other words, even though people use the word "burn", the process is not permanent. It is just another method for temporarily saving a file.

