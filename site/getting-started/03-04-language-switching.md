## Fast Language Switching

If you know more than one language, you can quickly switch between them with Fenrir. Press the Fenrir script key. The Script key is the Super key located between the left control and alt keys. Your Super key may have the Windows logo on it.

To add a new language, or remove one, open "Configure fast language switching" located under the "Settings" menu.

