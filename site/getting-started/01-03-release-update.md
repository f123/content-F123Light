## Updating the F123Light Operating System

In the "Main Menu" there is a sub-menu called "Settings", which has an option that makes it easy to update the system, called: "Check for system updates"

Thanks to this option, it is usually not necessary to flash a new image or use complicated commands whenever a new version of F123Light is available. However, we do recommend that you stay in touch with our team, as at this stage in the project, new versions are sometimes experimental, and are not recommended for all users.

If you are a technically-oriented or advanced computer user, you might want to explore the more advanced options described in [our technical guide](advanced.html).

